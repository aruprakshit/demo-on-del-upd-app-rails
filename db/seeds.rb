# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

c1 = Company.create(name: 'Test 1')
c2 = Company.create(name: 'Test 1')

u1 = User.create(name: 'User 1')
u2 = User.create(name: 'User 2')

ins1 = Inspection.create(name: 'Ins 1')
ins2 = Inspection.create(name: 'Ins 2')


req1 = MergeRequest.new
req1.company = c1
req1.user = u1
req1.inspection = ins1
req1.save

req2 = MergeRequest.new
req2.company = c2
req2.user = u2
req2.inspection = ins2
req2.save

ins3 = Inspection.create(name: 'Ins 4', merge_request: req1)
ins4 = Inspection.create(name: 'Ins 5', merge_request: req1)