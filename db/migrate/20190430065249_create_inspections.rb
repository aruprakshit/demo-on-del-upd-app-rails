class CreateInspections < ActiveRecord::Migration[5.2]
  def change
    create_table :inspections do |t|
      t.string :name

      t.timestamps
    end
  end
end
