class AddMergeRequestReferencesToInspections < ActiveRecord::Migration[5.2]
  def change
    add_reference :inspections,
                  :merge_request,
                  index: { name: 'inspector_tool_idx50_1' },
                  foreign_key: {
                    on_update: :cascade,
                    on_delete: :nullify,
                    name: 'inspector_tool_fk50_1'
                  },
                  null: true
  end
end
