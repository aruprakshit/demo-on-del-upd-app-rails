class CreateMergeRequests < ActiveRecord::Migration[5.2]
  def change
    create_table :merge_requests do |t|
      t.references :inspection,
                    index: { name: 'inspector_tool_idx52_3' },
                    foreign_key: {
                      on_update: :cascade,
                      on_delete: :cascade,
                      name: 'inspector_tool_fk52_3'
                    },
                    null: false
      t.references :user,
                   index: { name: 'inspector_tool_idx52_2' },
                   foreign_key: {
                    on_update: :cascade,
                    on_delete: :nullify,
                    name: 'inspector_tool_fk52_2'
                   },
                   null: true
      t.references :company,
                   index: { name: 'inspector_tool_idx52_1' },
                   foreign_key: {
                     on_update: :cascade,
                     on_delete: :cascade,
                     name: 'inspector_tool_fk52_1'
                   },
                   null: false

      t.timestamps
    end
  end
end
