# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_04_30_074754) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "companies", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "inspections", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "merge_request_id"
    t.index ["merge_request_id"], name: "inspector_tool_idx50_1"
  end

  create_table "merge_requests", force: :cascade do |t|
    t.bigint "inspection_id", null: false
    t.bigint "user_id"
    t.bigint "company_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["company_id"], name: "inspector_tool_idx52_1"
    t.index ["inspection_id"], name: "inspector_tool_idx52_3"
    t.index ["user_id"], name: "inspector_tool_idx52_2"
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "inspections", "merge_requests", name: "inspector_tool_fk50_1", on_update: :cascade, on_delete: :nullify
  add_foreign_key "merge_requests", "companies", name: "inspector_tool_fk52_1", on_update: :cascade, on_delete: :cascade
  add_foreign_key "merge_requests", "inspections", name: "inspector_tool_fk52_3", on_update: :cascade, on_delete: :cascade
  add_foreign_key "merge_requests", "users", name: "inspector_tool_fk52_2", on_update: :cascade, on_delete: :nullify
end
