class Inspection < ApplicationRecord
  has_many :merge_requests
  belongs_to :merge_request, optional: true
end
