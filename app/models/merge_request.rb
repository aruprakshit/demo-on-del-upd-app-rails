class MergeRequest < ApplicationRecord
  belongs_to :company
  belongs_to :inspection
  belongs_to :user

  has_many :source_inspection, class_name: 'Inspection'
end
